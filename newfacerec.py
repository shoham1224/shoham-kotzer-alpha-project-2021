import numpy as np
import dlib
import os
import cv2

a = {"picture": []}
for i in range(136):
    a[str(i)] = []
for filename in os.listdir("real"):
    exit=True
    # Load the detector
    detector = dlib.get_frontal_face_detector()
    # Load the predictor
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    # read the image
    img = cv2.imread("real\\" + filename)
    # Convert image into grayscale
    gray = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)
    # Use detector to find landmarks
    faces = detector(gray)
    print("Number of faces detected: {}".format(len(faces)))
    if(int(format(len(faces)))==0):
        exit = False
    elif(int(format(len(faces)))>1):
        for j in range(1, int(format(len(faces)))):
            a.setdefault("picture", []).append(format(filename)[len("database\\"): ] + "_%d" % j)
    if(exit):
        for face in faces:
            a.setdefault("picture", []).append(format(filename)[len("database\\"): ])
            x1 = face.left() # left point
            y1 = face.top() # top point
            x2 = face.right() # right point
            y2 = face.bottom() # bottom point
            # Create landmark object
            landmarks = predictor(image=gray, box=face)
            # Loop through all the points
            for n in range(0, 68):
                x = landmarks.part(n).x
                y = landmarks.part(n).y
                print("Part " + str(n) + ": {}".format(landmarks.part(n)), end=" - ")
                a.setdefault(str(n*2), []).append((x-x1)/(x2-x1))
                a.setdefault(str(n*2+1), []).append((y-y1)/(y2 - y1))
                print("(" + str((x-x1)/(x2-x1)) + "," + str((y-y1)/(y2 - y1)) + ")")
                # Draw a circle
                cv2.circle(img=img, center=(x, y), radius=3, color=(0, 255, 0), thickness=-1)
        # show the image
        #cv2.imshow(winname="Face", mat=img)
        # Delay between every fram
        #cv2.waitKey(delay=1)
        # Close all windows
        #cv2.destroyAllWindows()
print(len(a["picture"]), end=" ")
for i in range(136):
    print(len(a[str(i)]), end=" ")



print(a.keys())

b = pd.DataFrame(a)
print (b)
b.to_csv("cour_real_face.csv", sep=' ', index=False)

x = open("cour_real_face.csv" )
s=x.read().replace(" ", "," )
x.close()

#//Now open the file in write mode and write the string with replaced

x=open("cour_real_face.csv","w")
x.write(s)
x.close
